import sys

from typing import Dict, List
from os import environ as env
from fire import Fire
from logging import info, basicConfig, getLogger

from google.cloud import bigquery, storage
from google.oauth2 import service_account


def download_blob_from_gcs(bucket_name, source_blob_name, destination_file_name) -> str:
    """Downloads a blob from the bucket."""
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(source_blob_name)

    blob.download_to_filename(destination_file_name)

    info('Blob {} downloaded to {}.'.format(
        source_blob_name,
        destination_file_name))

    return destination_file_name


def list_gcs_folder_contents(
    bucket_folder: str = "temp",
    bucket_name: str = "scpdi-experiments",
    project_id: str = "scp-api-1145",
    googleapis_keyfile: str = None
        ) -> list:
    storage_client = storage.Client()

    blobs = storage_client.list_blobs(bucket_name, prefix=bucket_folder)
    file_list = [blob.name for blob in blobs]
    return file_list


def upload_to_gcs(
    source_file_name: str,
    bucket_folder: str = "temp",
    bucket_name: str = "scpdi_experiments",
    project_id: str = "scp-api-1145",
    googleapis_keyfile: str = None
            ) -> str:

    destination_blob_name = None

    storage_client = storage.Client()

    bucket = storage_client.get_bucket(bucket_name)

    destination_blob_name = destination_blob_name or \
        f"""{bucket_folder}/{source_file_name}"""
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_filename(source_file_name)

    info('File {} uploaded to {}.'.format(
        source_file_name,
        destination_blob_name))

    return blob.id


if __name__ == "__main__":
    basicConfig(stream=sys.stdout, level=10)
    Fire({
        "list": list_gcs_folder_contents,
        "download": download_blob_from_gcs,
        "upload": upload_to_gcs})
    info("Complete.")
