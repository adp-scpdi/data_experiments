ENVIRONMENT_NAME=${COMPOSER_ENVIRONMENT}
LOCATION=${COMPOSER_LOCATION}

list()
{
  gcloud composer environments storage dags list \
    --environment $ENVIRONMENT_NAME \
    --location $LOCATION
}

install()
{
  gcloud composer environments storage dags import \
    --environment $ENVIRONMENT_NAME \
    --location $LOCATION \
    --source $LOCAL_FILE_TO_UPLOAD
}

while [ "$1" != "" ]; do
  case $1 in
    -l | --list)      list
                      exit
                      ;;
    -i | --install)   shift
                      LOCAL_FILE_TO_UPLOAD=$1
                      install
                      exit
                      ;;
    * )               usage
                      exit 1
  esac
  shift
done
